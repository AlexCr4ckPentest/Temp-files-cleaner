#pragma once

#include <memory>

#include "impl/files_cleaner_impl.hpp"



namespace alex::utils
{
  class files_cleaner
  {
  public:
    files_cleaner()
      : impl_{std::make_unique<impl::files_cleaner_impl>()}
    { }

    files_cleaner(std::string_view target_folder)
      : impl_{std::make_unique<impl::files_cleaner_impl>(target_folder)}
    { }

    ~files_cleaner() = default;

    // No copy, no move
    files_cleaner(files_cleaner&&) = delete;
    files_cleaner(const files_cleaner&) = delete;
    files_cleaner& operator=(const files_cleaner&) = delete;
    files_cleaner& operator=(const files_cleaner&&) = delete;

    void set_target_directory(const fs::path& target_dir)
    { impl_->set_target_directory(target_dir); }

    std::string current_target_directory() const
    { return impl_->current_target_directory(); }

    std::size_t remove_all()
    { return impl_->remove_all(); }

    auto list_of_entities_to_remove() const
    { return impl_->list_of_entities_to_remove(); }

  private:
    const std::unique_ptr<impl::files_cleaner_impl> impl_;
  };
} // namespace alex::utils
